#!/usr/bin/python
import sys
import cv2
import numpy as np

from PyQt5 import QtCore
from PyQt5 import QtWidgets
from PyQt5 import QtGui, uic
from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtGui import QIcon
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QLabel, QGridLayout, QWidget, QComboBox
from PyQt5.QtCore import QSize, QRect
from PyQt5.QtWidgets import (QMainWindow, QTextEdit,
    QAction, QFileDialog, QApplication)

import face_recognition

from VOICE_INTERACTION import Speak
from BADGE_GENERATOR import Badge_Generator
from PERSON import Person

class RecordVideo(QtCore.QObject):
    image_data = QtCore.pyqtSignal(np.ndarray)

    def __init__(self, camera_port=1, parent=None):
        super().__init__(parent)
        self.camera = cv2.VideoCapture(camera_port)
        self.timer = QtCore.QBasicTimer()

    def start_recording(self):
        self.timer.start(0, self)

    def timerEvent(self, event):
        if (event.timerId() != self.timer.timerId()):
            return

        read, data = self.camera.read()
        if read:
            self.image_data.emit(data)



class FaceDetectionWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.image = QtGui.QImage()
        self._red = (0, 0, 255)
        self._width = 2
        self._min_size = (30, 30)

        self.unknown_delay = 0
        self.face_locations = []
        self.face_encodings = []
        self.face_names = []

        self.process_this_frame = True
        self.database_folder = '/home/aie/Bureau/FACE_RECOGNITION_SYSTEM/DATABASE/'
        self.guests_list = '/home/aie/Bureau/FACE_RECOGNITION_SYSTEM/LIST.txt'
        self.persons_list = []
        self.confirmation_delay = 8
        self.unknown_delay_limit = 20
        self.unknown_list = []

        self.logo_cap = cv2.imread('/home/aie/Bureau/FACE_RECOGNITION_SYSTEM/LOGOS/capgemini.png')
        self.logo_aie = cv2.imread('/home/aie/Bureau/FACE_RECOGNITION_SYSTEM/LOGOS/AIE.png')
        self.logo = QtGui.QImage('/home/aie/Bureau/FACE_RECOGNITION_SYSTEM/LOGOS/capgemini.png','png')
        self.center_x = int(640 / 2)
        self.center_y = int(480 / 2)
        self.rect_width = 300
        self.rect_height = 300

        self.read_guests_list()

        self.generate_badges = False
        self.voice = True
        # OPTIONS
        if (self.voice):
            self.Voice = Speak()
            self.Voice.start()
            self.Voice.list = self.unknown_list

        if (self.generate_badges):
            self.badge_generator = Badge_Generator()
            self.badge_generator.start()


    def read_guests_list(self):# READ LIST OF GUESTS
        with open(self.guests_list) as f:
            for line in f:
                splitted = line.split(';')
                print(splitted)
                self.persons_list.append(Person(splitted[0], splitted[1], splitted[2], splitted[3], self.database_folder))

                if self.persons_list[-1].face is None:
                    self.unknown_list.append(self.persons_list[-1].name)


    def detect_faces(self, image:np.ndarray):

        cv2.flip(image, 1, image)
        small_frame = cv2.resize(image, (0, 0), fx=0.25, fy=0.25)

        if self.process_this_frame:
            self.face_locations = face_recognition.face_locations(small_frame)
            self.face_encodings = face_recognition.face_encodings(small_frame, self.face_locations)
            self.face_names = []
            for face_encoding in self.face_encodings:
                match = False
                for person in self.persons_list:
                    if person.face is not None:
                        match = face_recognition.compare_faces([person.face], face_encoding)
                        if match[0]:
                            self.face_names.append(person.name)
                            person.is_here = True
                            if person.waiting_to_confirm == self.confirmation_delay and person.welcomed is False:
                                if (self.voice):
                                    if not self.Voice.running:
                                        person.welcomed = True
                                        self.Voice.welcoming = True
                                        self.Voice.speech = person.speech
                                        self.Voice.function = person.function
                                        self.Voice.company = person.company
                                        self.Voice.name = person.name

                                if (self.generate_badges):
                                    self.badge_generator.face = person.picture_file
                                    self.badge_generator.name = person.name
                                    self.badge_generator.company = person.company
                                    self.badge_generator.function = person.function
                                    self.badge_generator.generate_badge = True
                            else:
                                person.waiting_to_confirm += 1
                            match = True
                            break
                        else:
                            person.is_here = False

                if match == [False]:
                    self.face_names.append('Unknown')
                    if self.unknown_delay == self.unknown_delay_limit:
                        self.Voice.interacting = True
                        self.unknown_delay = 0
                    else:
                        self.unknown_delay += 1
                else:
                    self.unknown_delay = 0

        self.process_this_frame = not self.process_this_frame


    def image_data_slot(self, image_data):

        self.detect_faces(image_data)

        for (top, right, bottom, left), name in zip(self.face_locations, self.face_names):
            top *= 4
            right *= 4
            bottom *= 4
            left *= 4

            color = (0, 0, 255)

            for person in self.persons_list:
                if not person.is_here:
                    person.waiting_to_confirm = 0

                if person.name == name and person.welcomed:
                    color = (0, 255, 0)

            cv2.rectangle(image_data, (left, top), (right, bottom), color, 2)
            cv2.rectangle(image_data, (left, bottom - 35), (right, bottom), color, cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX

            split = 0
            for split in range(0, len(name)):
                if name[split] is ' ':
                    break
            firstpart, secondpart = name[:split + 1], name[split + 1:]

            cv2.putText(image_data, firstpart, (left + 6, bottom - 20), font, .4, (255, 255, 255), 1)
            cv2.putText(image_data, secondpart, (left + 6, bottom - 6), font, .4, (255, 255, 255), 1)

        #if (len(self.face_names) is 0):
            #cv2.rectangle(image_data, (self.center_x - int(self.rect_width / 2), self.center_y - int(self.rect_height / 2)),
                          #(self.center_x + int(self.rect_width / 2), self.center_y + int(self.rect_height / 2)), (255, 255, 255), 2)
            #cv2.putText(image_data, "PLACE YOUR FACE IN THIS SQUARE",
                        #(self.center_x - int(self.rect_height / 2) - 20, self.center_y + int(self.rect_height / 2) + 20),
                        #cv2.FONT_HERSHEY_DUPLEX, .4, (255, 255, 255), 1)

        self.image = self.get_qimage(image_data)
        self.image = self.image.scaled(self.size().width(), self.size().width()*3/4) # WEBCAM RATIO
        self.add_logo()# AJOUT DU LOGO

        #self.setFixedSize(self.image.size())
        #if self.image.size() != self.size():
            #self.setFixedSize(self.image.size())

        self.update()

    def add_logo(self, scale = 6):# AJOUT DU LOGO
        painter = QtGui.QPainter()
        painter.begin(self.image)
        logo_size = self.logo.size()
        logo_reduced = self.logo.scaled(self.size().width() / scale, self.size().width() / scale * logo_size.height() / logo_size.width())
        painter.drawImage(0, self.size().height() - logo_reduced.size().height(), logo_reduced)
        painter.end()

    def get_qimage(self, image: np.ndarray):
        height, width, colors = image.shape
        bytesPerLine = 3 * width
        QImage = QtGui.QImage

        image = QImage(image.data,
                       width,
                       height,
                       bytesPerLine,
                       QImage.Format_RGB888)

        image = image.rgbSwapped()
        return image

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.drawImage(0, 0, self.image)
        self.image = QtGui.QImage()


class ExampleWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        self.setMinimumSize(QSize(640, 140))
        self.setWindowTitle("Combobox example")

        centralWidget = QWidget(self)
        self.setCentralWidget(centralWidget)

        # Create combobox and add items.
        self.comboBox = QComboBox(centralWidget)
        self.comboBox.setGeometry(QRect(40, 40, 491, 31))
        self.comboBox.setObjectName(("comboBox"))
        self.comboBox.addItem("PyQt")
        self.comboBox.addItem("Qt")
        self.comboBox.addItem("Python")
        self.comboBox.addItem("Example")
        self.show()




class MainWidget(QWidget):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):

        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHeightForWidth(True)
        self.setSizePolicy(sizePolicy)

        self.setGeometry(300, 300, 300, 220)
        self.setWindowTitle('Face Recognition System')
        self.setWindowIcon(QIcon('/home/aie/Bureau/FACE_RECOGNITION_SYSTEM/LOGOS/capgemini.png'))

        self.face_detection_widget = FaceDetectionWidget()
        self.record_video = RecordVideo()

        image_data_slot = self.face_detection_widget.image_data_slot
        self.record_video.image_data.connect(image_data_slot)

        layout = QtWidgets.QVBoxLayout()



        layout.addWidget(self.face_detection_widget)


        # ADD START BUTTON
        '''
        self.run_button = QtWidgets.QPushButton('Start')
        layout.addWidget(self.run_button)
        self.run_button.clicked.connect(self.record_video.start_recording)
        '''

        self.record_video.start_recording()


        self.setLayout(layout)
        self.show()


    def open(self):
        fileName = QFileDialog.getOpenFileName(self, 'OpenFile', '/home')
        self.myTextBox.setText(fileName)
        print(fileName)




if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)


    main_widget = MainWidget()
    #main_window = ExampleWindow()



    #main_widget = MainWidget()
    #main_widget.setWindowTitle('Face Recognition System')

    #main_window = QtWidgets.QMainWindow()
    #main_window.setCentralWidget(main_widget)
    #main_window.show()

    #main_widget.show()

    sys.exit(app.exec_())