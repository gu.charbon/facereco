from threading import Thread
import cv2
import numpy as np

class Badge_Generator(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.margin = 15
        self.picture_size = 200
        self.height = 300
        self.width = 685

        self.page_height = self.height#int(3508/2)
        self.page_width = self.width#int(2480/2)

        self.generate_badge = False
        self.face = None
        self.company = ''
        self.function = ''
        self.name = ''

    def generate(self, face, name, company, function):
        logo_cap = cv2.imread('LOGOS/capgemini.png')
        logo_aie = cv2.imread('LOGOS/AIE.png')

        badge = np.ones((self.page_height, self.page_width, 3), np.uint8)*255
        face_picture = cv2.imread(face)

        face_picture_small = np.ones((self.picture_size, self.picture_size, 3), np.uint8)*255
        cv2.resize(face_picture, (self.picture_size, self.picture_size), face_picture_small)

        badge[self.margin:self.margin+face_picture_small.shape[0], self.margin:self.margin+face_picture_small.shape[0]] = face_picture_small

        badge[2*self.margin+self.picture_size:2*self.margin+self.picture_size + logo_cap.shape[0], self.margin:self.margin + logo_cap.shape[1]] = logo_cap
        badge[1*self.margin+self.picture_size:1*self.margin+self.picture_size + logo_aie.shape[0], self.width - self.margin - logo_aie.shape[1]:self.width - self.margin] = logo_aie

        cv2.putText(badge, name, (face_picture_small.shape[0]+2*self.margin, 4*self.margin), cv2.FONT_HERSHEY_DUPLEX, 1.0, (0, 0, 0), 2)

        cv2.putText(badge, company, (face_picture_small.shape[0]+2*self.margin, 7*self.margin), cv2.FONT_HERSHEY_DUPLEX, .75,(0, 0, 0), 2)

        limit = 35
        split = 0
        if(len(function) > limit):
            for split in range(0,len(function)-limit):
                if function[limit + split] is ' ':
                    break
            firstpart, secondpart = function[:limit+split+1], function[limit+split+1:]
            cv2.putText(badge, firstpart, (face_picture_small.shape[0]+2*self.margin, 10*self.margin), cv2.FONT_HERSHEY_DUPLEX,.6, (0, 0, 0), 1)
            if (len(secondpart) > limit):
                for split in range(0, len(secondpart) - limit):
                    if secondpart[limit + split] is ' ':
                        break

                secondpart, thirdpart = secondpart[:limit + split], secondpart[limit + split + 1:]
                cv2.putText(badge, secondpart, (face_picture_small.shape[0] + 2 * self.margin, 12 * self.margin),cv2.FONT_HERSHEY_DUPLEX, .6, (0, 0, 0), 1)
                cv2.putText(badge, thirdpart, (face_picture_small.shape[0] + 2 * self.margin, 14 * self.margin),cv2.FONT_HERSHEY_DUPLEX, .6, (0, 0, 0), 1)
            else:
                cv2.putText(badge, secondpart, (face_picture_small.shape[0] + 2 * self.margin, 12 * self.margin),cv2.FONT_HERSHEY_DUPLEX, .6, (0, 0, 0), 1)





        else:
            cv2.putText(badge, function, (face_picture_small.shape[0] + 2 * self.margin, 10 * self.margin),cv2.FONT_HERSHEY_DUPLEX, .6, (0, 0, 0), 1)

        cv2.rectangle(badge, (0, 0), (self.width, self.height), (0, 0, 0), 2)

        cv2.imwrite('BADGES/'+name+'.jpg',badge)

        #cv2.namedWindow('Badge of ' + name)
        #cv2.imshow('Badge of ' + name, badge)
        #cv2.waitKey(1)
        #cv2.destroyWindow('Badge of ' + name)

    def run(self):
        while True:
            if self.generate_badge:
                self.generate(self.face, self.name, self.company, self.function)
                self.generate_badge = False

