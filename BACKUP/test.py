import face_recognition
import cv2



video_capture = cv2.VideoCapture(0)
cv2.namedWindow('Facial Recognition System', 0)



while True:
    ret, frame = video_capture.read()
    cv2.flip(frame, 1, frame)
    small_frame = cv2.resize(frame, (0, 0), fx=0.1, fy=0.1)

   
    # Display the resulting image
    cv2.imshow('Facial Recognition System', frame)

    # Hit 'q' on the keyboard to quit!
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release handle to the webcam
video_capture.release()
cv2.destroyAllWindows()
