from threading import Thread
import speech_recognition as sr
import os
import cv2

# INIT MICROPHONE
mic_name = "HD Webcam C615: USB Audio (hw:1,0)"
sample_rate = 48000
chunk_size = 2048
r = sr.Recognizer()
mic_list = sr.Microphone.list_microphone_names()
print(mic_list)
device_id = 0
for i, microphone_name in enumerate(mic_list):
    if microphone_name == mic_name:
        device_id = i

# wait for a second to let the recognizer adjust the
# energy threshold based on the surrounding noise level
print('Adjusting Microphone Noise...')
with sr.Microphone(device_index=device_id, sample_rate=sample_rate, chunk_size=chunk_size) as source:
    r.adjust_for_ambient_noise(source)


class Speak(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.welcoming = False
        self.interacting = False
        self.list = []
        self.running = False
        self.match = False
        self.capture = False
        self.recognized = None
        self.speech = ''
        self.company = ''
        self.function = ''
        self.name = ''

    def welcome(self):
        self.running = True
        s = "google_speech -l en \"Hi " + self.name + ". Welcome to Capgemini.\" -e speed 1.05"
        os.system(s)
        s = "google_speech -l en \"You are currently " + self.function + " at " + self.company + "\" -e speed 1.05"
        os.system(s)
        self.welcoming = False
        self.running = False

    def interact(self):
        self.running = True

        with sr.Microphone(device_index=device_id, sample_rate=sample_rate, chunk_size=chunk_size) as source:

            for name in self.list:
                s = "google_speech -l en \"Are you " + name + "? YES or NO ?\" -e speed 1.05"
                os.system(s)

                self.result = 2
                while (self.result == 2):
                        audio = r.listen(source, None, 1.5)
                        try:
                            text = r.recognize_google(audio)

                            print(text)
                            #s = "google_speech -l en \"You said " + text + "\" -e speed 1.05"
                            #os.system(s)

                            if (text == "yes"):
                                self.result = 1
                                break
                            elif (text == "no"):
                                self.result = 0
                                break
                            elif (text == "break"):
                                self.result = 3
                                break
                            else:
                                self.result = 2

                        except sr.UnknownValueError:
                            s = "google_speech -l en \"Sorry, I have not understood ! Are you " + name + "? Yes or No?\" -e speed 1.05"
                            os.system(s)
                            self.result = 2

                if (self.result == 1):
                    s = "google_speech -l en \"OK! Nice to meet you!\" -e speed 1.05"
                    os.system(s)
                    self.recognized = name

                    s = "google_speech -l en \"Please stand in front of the camera.\" -e speed 1.05"
                    os.system(s)

                    self.capture = True
                    while(self.capture):
                        cv2.waitKey(10)

                    s = "google_speech -l en \"Thank you.\" -e speed 1.05"
                    self.recognized = None
                    os.system(s)
                    self.match = True
                    break
                elif (self.result == 3):
                    s = "google_speech -l en \"All right!\" -e speed 1.05"
                    self.match = True
                    os.system(s)
                    break

        if(self.match == False):
            s = "google_speech -l en \"Sorry, but you are not in the guest list...!\" -e speed 1.05"
            os.system(s)
        self.interacting = False
        self.running = False

    def run(self):
        while True:
            #print('running voice interaction...')
            if self.welcoming and not self.running:
                self.welcome()
            elif self.interacting and not self.running:
                self.interact()



